# Graphical user interface

## User directories

Install the `xdg-user-dirs` package.

Create English-named directories:

```shell
$ LC_ALL=C xdg-user-dirs-update --force
```

## Fonts

Install the following packages:

- noto-fonts
   - noto-fonts-cjk
   - noto-fonts-emoji
- ttf-dejavu

## Audio server

Install the following packages:

- pipewire
   - pipewire-alsa
   - pipewire-jack
   - pipewire-pulse
   - wireplumber

## Display server

Install the following packages:

- xorg-server
- xorg-xrandr

## Display drivers

See [Display drivers][archwiki-display-drivers] for details.

If you install Arch Linux on VirtualBox, see the _Guest additions_ section in
[VirtualBox](../../virtualbox.md) for details.

## Graphical environment

- Window manager
   - [Dwm](dwm.md)

## References

1. [XDG user directories][xdg-user-directories]
2. [Display drivers][archwiki-display-drivers]

[archwiki-display-drivers]: https://wiki.archlinux.org/title/General_recommendations#Display_drivers
[xdg-user-directories]: https://wiki.archlinux.org/title/XDG_user_directories
