# Dwm

## Xinit

Install the `xorg-xinit` package.

### Configuration

Create a copy of the default configuration:

```shell
$ cp /etc/X11/xinit/xinitrc ~/.xinitrc
```

Edit the `~/.xinitrc` file, find the following lines:

```
twm &
xclock -geometry 50x50-1+1 &
xterm -geometry 80x50+494+51 &
xterm -geometry 80x20+494-0 &
exec xterm -geometry 80x66+0+0 -name login
```

Replace them with:

```
[ -f /etc/xprofile ] && . /etc/xprofile
[ -f "$HOME/.xprofile" ] && . "$HOME/.xprofile"

exec dwm
```

## Components

The current setup consists of the following:

- Window manager: [dwm][my-build-dwm]
   - Run launcher: dmenu
   - Status monitor: slstatus
   - Terminal emulator: [st][my-build-st]

## Graphical programs

Install the following programs, see
[List of programs](../../list-of-programs.md) for details:

- GTK configuration tool
- Notification daemon
- Polkit authentication agent
- Network applet
- Volume applet
- Wallpaper setting utility
- Input method
- File manager
- Text editor
- Web browser

## Graphical interface

Start display server:

```shell
$ startx
```

### Screen resolution

List all available outputs, screen resolutions and refresh rates:

```shell
$ xrandr
```

Set the screen resolution and refresh rate, run the following line, then add it
to the `~/.xprofile` file:

```
xrandr --output <output> --mode <resolution> --rate <refresh_rate>
```

### Theming

Run the GTK configuration tool to set the theme, font and icon.

## References

1. [Xinit][archwiki-xinit]
2. [Xrandr][xrandr]

[archwiki-xinit]: https://wiki.archlinux.org/title/Xinit
[my-build-dwm]: https://gitlab.com/thvdw223/dwm
[my-build-st]: https://gitlab.com/thvdw223/st
[xrandr]: https://wiki.archlinux.org/title/Xrandr
