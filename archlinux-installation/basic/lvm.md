# LVM

The following instructions use `volgroup0` as the example volume group with
`root.lv` as the logical volume for `\` and `home.lv` as the logical volume
for `\home`.

Start partitioning with `fdisk`:

```
# fdisk /dev/<the_disk_to_be_partitioned>
```

## Layout

The following layouts use `/dev/sda` as the example disk with `/dev/sda1`
as the first partition.

### BIOS/MBR

| Mount point | Partition | Type       | Boot flag | Size                    |
|:------------|:----------|:-----------|:---------:|:------------------------|
| None        | /dev/sda1 | Linux LVM  | Yes       | All the free space left |

### UEFI/GPT

| Mount point | Partition | Type       | Size                    |
|:------------|:----------|:-----------|:------------------------|
| /efi        | /dev/sda1 | EFI system | 512M                    |
| None        | /dev/sda2 | Linux LVM  | All the free space left |

### Logical volumes

| Mount point | Logical volume    | Size                  |
|:------------|:------------------|:----------------------|
| /           | volgroup0/root.lv | 30G                   |
| /home       | volgroup0/home.lv | 90% of the free space |

## Partitioning

### BIOS/MBR

Create a new MBR partition table:

```
Command (m for help): o
```

Create a LVM partition:

```
Command (m for help): n
Select (default p):
Partition number (default 1):
First sector (default):
Last sector (default):

Command (m for help): t
Hex code or alias (type L to list all): lvm
```

Make the partition bootable:

```
Command (m for help): a
```

Print the partition table:

```
Command (m for help): p
```

Write the table to disk:

```
Command (m for help): w
```

### UEFI/GPT

Create a new GPT partition table:

```
Command (m for help): g
```

Create a EFI system partition:

```
Command (m for help): n
Partition number (default 1):
First sector (default):
Last sector (default): +512M

Command (m for help): t
Partition type or alias (type L to list all): uefi
```

Create a LVM partition:

```
Command (m for help): n
Partition number (default 2):
First sector (default):
Last sector (default):

Command (m for help): t
Partition number (default 2):
Partition type or alias (type L to list all): lvm
```

Print the partition table:

```
Command (m for help): p
```

Write the table to disk:

```
Command (m for help): w
```

## Physical volume

Create a new physical volume:

```
# pvcreate --dataalignment 1m /dev/<lvm_partition>
```

Display detailed information of physical volume:

```
# pvdisplay
```

Display summary information of physical volume:

```
# pvs
```

Remove a physical volume, make sure it is not used by any volume group:

```
# pvremove /dev/<lvm_partition>
```

## Volume group

Create a new volume group:

```
# vgcreate <group_name> /dev/<lvm_partition>
```

Display detailed information of volume group:

```
# vgdisplay
```

Display summary information of volume group:

```
# vgs
```

Remove a volume group, make sure there is no logical volume in it:

```
# vgremove <group_name>
```

## Logical volume

Create two logical volumes:

```
# lvcreate -L 30G <group_name> -n root.lv
# lvcreate -l 90%FREE <group_name> -n home.lv
```

Display detailed information of logical volume:

```
# lvdisplay
```

Display summary information of logical volume:

```
# lvs
```

Remove a logical volume:

```
# lvremove <group_name>/<logical_volume>
```

## Make volume groups available

Load the `dm_mod` kernel module:

```
# modprobe dm_mod
```

Scan volume groups:

```
# vgscan
```

Activate volume groups:

```
# vgchange -ay
```

## Format and mount logical volumes

### Root (/)

Format `root.lv`:

```
# mkfs.ext4 /dev/<group_name>/root.lv
```

Mount `root.lv`:

```
# mount /dev/<group_name>/root.lv /mnt
```

### Home (/home)

Format `home.lv`:

```
# mkfs.ext4 /dev/<group_name>/home.lv
```

Create the `/mnt/home` directory, then mount `home.lv`:

```
# mount /dev/<group_name>/home.lv /mnt/home
```

## Format and mount EFI system partition (UEFI/GPT)

Format partition:

```
# mkfs.vfat /dev/<efi_system_partition>
```

Create the `/mnt/efi` directory, then mount partition:

```
# mount /dev/<efi_system_partition> /mnt/efi
```

## References

1. [Partitioning][archwiki-partitioning]
2. [Install Arch Linux on LVM][install-on-lvm]

[archwiki-partitioning]: https://wiki.archlinux.org/title/Partitioning
[install-on-lvm]: https://wiki.archlinux.org/title/Install_Arch_Linux_on_LVM
