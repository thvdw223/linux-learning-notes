# GRUB

## Installation

### BIOS

Install the `grub` package. Then do:

```
# grub-install --target=i386-pc /dev/<the_disk_to_be_installed>
```

### UEFI

Install the following packages:

- efibootmgr
- grub

Make sure the EFI system partition is mounted on the `/efi` directory. Then do:

```
# grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB
```

## Boot message

Edit the `/etc/default/grub` file, find the following line:

```
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet"
```

Delete `quiet`:

```
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3"
```

Re-generate the grub configuration.

## Generate configuration

Generate a GRUB configuration file:

```
# grub-mkconfig -o /boot/grub/grub.cfg
```

## References

1. [GRUB][archwiki-grub]

[archwiki-grub]: https://wiki.archlinux.org/title/GRUB
