# Basic installation

The following instructions are included my personal preferences so some of them
are different from [Installation guide][installation-guide].

## Bigger font

See the _Have a bigger font_ section in [Console fonts](../../console-fonts.md)
for details.

## Verify the boot mode

To verify the boot mode, list the `efivars` directory:

```
# ls /sys/firmware/efi/efivars
```

If the directory exists, then the system is booted in UEFI mode. if not, the
system is booted in BIOS mode.

## Verify the internet connection

The connection may be verified with:

```
# ping archlinux.org
```

## Update the system clock

Ensure the system clock is accurate:

```
# timedatectl set-ntp true
```

## Set up partitions

When recognized by the live system, disks are assigned to a block device such
as `/dev/sda`, `/dev/nvme0n1` or `/dev/mmcblk0`.

List all disks:

```
# lsblk
```

There are two ways to set up partitions:

- [Normal Partitioning](partitioning.md)
- [LVM](lvm.md)

## Install essential packages

Packages:

- base
- base-devel
- bash-completion
- git
- linux-firmware
- linux-lts
- linux-lts-headers
- man-db
- neovim

```
# pacstrap /mnt [packages...]
```

## Fstab

Update information about filesystems into the new system:

```
# genfstab -U /mnt >> /mnt/etc/fstab
```

## Chroot

Change root into the new system:

```
# arch-chroot /mnt
```

## PC speaker

Unload the `pcspkr` kernel module:

```
# rmmod pcspkr
```

Create the `/etc/modprobe.d/nobeep.conf` file, add the following line:

```
blacklist pcspkr
```

## Pacman configuration

See the _Configuration_ section in [Pacman](../../pacman.md) for details.

## Time zone

Set the time zone:

```
# ln -fs /usr/share/zoneinfo/Asia/Taipei /etc/localtime
```

Set the hardware clock from the system clock:

```
# hwclock -w
```

## Localization

Edit the `/etc/locale.gen` file, find the following locales and uncomment them:

- `en_US.UTF-8 UTF-8`
- `zh_TW.UTF-8 UTF-8`

Generate the locales:

```
# locale-gen
```

Create the `/etc/locale.conf` file, set the `LANG` variable accordingly:

```
LANG=en_US.UTF-8
```

## Font configuration

See [Console fonts](../../console-fonts.md) for details.

## Hostname

Create the `/etc/hostname` file, set hostname accordingly:

```
myhostname
```

## Initramfs (for LVM)

Install the `lvm2` package.

Edit the `/etc/mkinitcpio.conf` file, find the following line:

```
HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)
```

Add `lvm2` between `block` and `filesystems`:

```
HOOKS=(base udev autodetect modconf block lvm2 filesystems keyboard fsck)
```

Recreate the initramfs image:

```
# mkinitcpio -P
```

## Bootloader

See [GRUB](grub.md) for details.

## Networking

See the _Installation_ section in [NetworkManager](../../networkmanager.md) for
details.

## Sudo

See [Sudo](../../sudo.md) for details.

Disable root login:

```
# passwd -l root
```

## Create user account

Create a user:

```
# useradd -m -G wheel <username>
```

Set password for the user:

```
# passwd <username>
```

## Additional packages

Packages:

- bat
- exa
- htop
- lynx
- ncdu
- pacman-contrib

## Finish the installation

Exit the chroot environment:

```
# exit
```

Unmount all the partitions:

```
# umount -R /mnt
```

Shutdown the machine:

```
# poweroff
```

Remove the installation medium and start the machine.

## Post-installation

### Time synchronization

Enable network time synchronization service:

```shell
$ sudo timedatectl set-ntp true
```

### Restore dotfiles if any

See the _Restore_ section in [Dotfiles](../../dotfiles.md) for details.

### Editing files

Set the `EDITOR` variable accordingly:

```shell
$ export EDITOR=nvim
```

Use `sudoedit` to edit system configuration files.

### Set up a swap file if necessary

See [Swap file](../../swap-file.md) for details.

### Firewall

- [Firewalld](../../firewalld.md)
- [Uncomplicated firewall](../../uncomplicated-firewall.md)

### Reflector

See [Reflector](../../reflector.md) for details.

### Use Dash as /bin/sh

See [Dash](../../dash.md) for details.

### Use Zsh as your default shell if necessary

See [Zsh](../../zsh.md) for details.

## References

1. [Installation guide][installation-guide]
2. [Disable PC Speaker][disable-pc-speaker]
3. [Adding mkinitcpio hooks][mkinitcpio-hooks]

[disable-pc-speaker]: https://wiki.archlinux.org/title/PC_speaker#Disable_PC_Speaker
[installation-guide]: https://wiki.archlinux.org/title/Installation_guide
[mkinitcpio-hooks]: https://wiki.archlinux.org/title/Install_Arch_Linux_on_LVM#Adding_mkinitcpio_hooks
