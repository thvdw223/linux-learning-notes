# Partitioning

Start partitioning with `fdisk`:

```
# fdisk /dev/<the_disk_to_be_partitioned>
```

## Layout

The following layouts use `/dev/sda` as the example disk with `/dev/sda1`
as the first partition.

### BIOS/MBR

| Mount point | Partition | Type       | Boot flag | Size                    |
|:------------|:----------|:-----------|:---------:|:------------------------|
| [SWAP]      | /dev/sda1 | Linux swap | No        | 1G                      |
| /           | /dev/sda2 | Linux      | Yes       | All the free space left |

### UEFI/GPT

| Mount point | Partition | Type                | Size                    |
|:------------|:----------|:--------------------|:------------------------|
| /efi        | /dev/sda1 | EFI system          | 512M                    |
| [SWAP]      | /dev/sda2 | Linux swap          | 1G                      |
| /           | /dev/sda3 | Linux root (x86-64) | All the free space left |

## BIOS/MBR

Create a new MBR partition table:

```
Command (m for help): o
```

Create a swap partition:

```
Command (m for help): n
Select (default p):
Partition number (default 1):
First sector (default):
Last sector (default): +1G

Command (m for help): t
Hex code or alias (type L to list all): swap
```

Create a root partition:

```
Command (m for help): n
Select (default p):
Partition number (default 2):
First sector (default):
Last sector (default):
```

Make the root partition bootable:

```
Command (m for help): a
Partition number (default 2):
```

Print the partition table:

```
Command (m for help): p
```

Write the table to disk:

```
Command (m for help): w
```

## UEFI/GPT

Create a new GPT partition table:

```
Command (m for help): g
```

Create a EFI system partition:

```
Command (m for help): n
Partition number (default 1):
First sector (default):
Last sector (default): +512M

Command (m for help): t
Partition type or alias (type L to list all): uefi
```

Create a swap partition:

```
Command (m for help): n
Partition number (default 2):
First sector (default):
Last sector (default): +1G

Command (m for help): t
Partition number (default 2):
Partition type or alias (type L to list all): swap
```

Create a root partition:

```
Command (m for help): n
Partition number (default 3):
First sector (default):
Last sector (default):

Command (m for help): t
Partition number (default 3):
Partition type or alias (type L to list all): 23
```

Print the partition table:

```
Command (m for help): p
```

Write the table to disk:

```
Command (m for help): w
```

## Format and mount partitions

### Swap

Format it to swap:

```
# mkswap -L SWAP /dev/<swap_partition>
```

Enable swap:

```
# swapon /dev/disk/by-label/SWAP
```

### Root (/) 

Format partition:

```
# mkfs.ext4 -L ROOT /dev/<root_partition>
```

Mount partition:

```
# mount /dev/disk/by-label/ROOT /mnt
```

### EFI system partition

Format partition:

```
# mkfs.vfat /dev/<efi_system_partition>
```

Label the EFI partition:

```
# fatlabel /dev/<efi_system_partition> EFI
```

Create the `/mnt/efi` directory, then mount partition:

```
# mount /dev/disk/by-label/EFI /mnt/efi
```

## References

1. [Partitioning][archwiki-partitioning]

[archwiki-partitioning]: https://wiki.archlinux.org/title/Partitioning
