# Firewalld

## Installation

Install the `firewalld` package.

Enable and start service:

```
# systemctl enable --now firewalld.service
```

## Logging

Enable logging:

```
# firewall-cmd --set-log-denied=all
```

## Zone

List all zones:

```
# firewall-cmd --get-zones
```

List all zones with more information:

```
# firewall-cmd --list-all-zones
```

Show default zone:

```
# firewall-cmd --get-default-zone
```

Set the `home` zone as default zone:

```
# firewall-cmd --set-default-zone=home
```

Show active zones:

```
# firewall-cmd --get-active-zones
```

## Service

List services in the `public` zone:

```
# firewall-cmd --zone=public --list-services
```

List all available services:

```
# firewall-cmd --get-services
```

Show information about the `ssh` service:

```
# firewall-cmd --info-service=ssh
```

Add the `ssh` service to default zone and make it permanent:

```
# firewall-cmd --add-service=ssh
# firewall-cmd --permanent --add-service=ssh
```

## References

1. [Firewalld][archwiki-firewalld]

[archwiki-firewalld]: https://wiki.archlinux.org/title/Firewalld
