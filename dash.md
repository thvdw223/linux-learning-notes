# Dash

## Installation

Install the `dash` package.

## Use dash as /bin/sh

Relink `/bin/sh`:

```
# ln -fsT dash /usr/bin/sh
```

### Add pacman hook

Make sure the `/etc/pacman.d/hooks` directory exists.

Create the `/etc/pacman.d/hooks/relinking-bin-sh.hook` file, add the following
lines:

```
[Trigger]
Type = Package
Operation = Install
Operation = Upgrade
Target = bash

[Action]
Description = Re-pointing /bin/sh symlink to dash...
When = PostTransaction
Exec = /usr/bin/ln -fsT dash /usr/bin/sh
Depends = dash
```

## References

1. [Dash][archwiki-dash]

[archwiki-dash]: https://wiki.archlinux.org/title/Dash
