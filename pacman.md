# Pacman

## Configuration

Edit the `/etc/pacman.conf` file, find the following options and uncomment
them accordingly:

- Color
- VerbosePkgLists
- ParallelDownloads

## Reset all the keys

Delete the `/etc/pacman.d/gnupg` directory.

Initialize the keyring:

```
# pacman-key --init
```

Re-add the default keys:

```
# pacman-key --populate archlinux
```

## References

1. [Pacman][archwiki-pacman]
2. [Resetting all the keys][resetting-all-the-keys]

[archwiki-pacman]: https://wiki.archlinux.org/title/Pacman
[resetting-all-the-keys]: https://wiki.archlinux.org/title/Pacman/Package_signing#Resetting_all_the_keys
