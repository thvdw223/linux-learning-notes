# Uncomplicated firewall

## Installation

Install the `ufw` package.

## Basic configuration

Allow outgoing traffic by default:

```
# ufw default allow outgoing
```

Deny incoming traffic by default:

```
# ufw default deny incoming
```

## Application

List available applications:

```
# ufw app list
```

Allow rate-limited SSH traffic from anywhere:

```
# ufw limit SSH
```

## Enable firewall

Enable and start service:

```
# systemctl enable --now ufw.service
```

Enable ufw:

```
# ufw enable
```

## Query the rules

To see a list of numbered rules:

```
# ufw status numbered
```

To see extra information:

```
# ufw status verbose
```

## References

1. [Uncomplicated Firewall][ufw]

[ufw]: https://wiki.archlinux.org/title/Uncomplicated_Firewall
