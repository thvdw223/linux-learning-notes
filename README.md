# Linux learning notes

Here are the notes about what I learn while using [Arch Linux][archlinux].

## Table of contents

- Arch linux installation
   - [Basic](archlinux-installation/basic/README.md)
      - [GRUB](archlinux-installation/basic/grub.md)
      - [LVM](archlinux-installation/basic/lvm.md)
      - [Partitioning](archlinux-installation/basic/partitioning.md)
   - [Graphical user interface](archlinux-installation/graphical/README.md)
      - [Dwm](archlinux-installation/graphical/dwm.md)
- Artix linux installation
   - [Basic](artixlinux-installation/basic.md)
- [Console fonts](console-fonts.md)
- [Dash](dash.md)
- [Dotfiles](dotfiles.md)
- [Firefox](firefox.md)
- [Firewalld](firewalld.md)
- [Gcin](gcin.md)
- [List of programs](list-of-programs.md)
- [LVM snapshots](lvm-snapshots.md)
- [NetworkManager](networkmanager.md)
- [Pacman](pacman.md)
- [Reflector](reflector.md)
- [Sudo](sudo.md)
- [Swap file](swap-file.md)
- [Uncomplicated Firewall](uncomplicated-firewall.md)
- [Virt-manager](virt-manager.md)
- [VirtualBox](virtualbox.md)
- [Zsh](zsh.md)

[archlinux]: https://archlinux.org/
