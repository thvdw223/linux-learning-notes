# Basic installation

The following instructions are for Artix Linux installation with the OpenRC
init system.

## Set up partitions

See [Partitioning](../archlinux-installation/basic/partitioning.md) for
details.

## Install essential packages

Packages:

- base
- base-devel
- bash-completion
- elogind-openrc
- git
- linux-firmware
- linux-lts
- linux-lts-headers
- man-db
- neovim
- openrc

```
# basestrap /mnt [packages...]
```

## Fstab

Update information about filesystems into the new system:

```
# fstabgen -U /mnt >> /mnt/etc/fstab
```

## Chroot

Change root into the new system:

```
# artix-chroot /mnt
```

## Pacman configuration

See the _Configuration_ section in [Pacman](../pacman.md) for details.

## Time zone

Set the time zone:

```
# ln -fs /usr/share/zoneinfo/Asia/Taipei /etc/localtime
```

Set the hardware clock from the system clock:

```
# hwclock -w
```

## Localization

Edit the `/etc/locale.gen` file, find the following locales and uncomment them:

- `en_US.UTF-8 UTF-8`
- `zh_TW.UTF-8 UTF-8`

Generate the locales:

```
# locale-gen
```

Create the `/etc/locale.conf` file, set the `LANG` variable accordingly:

```
LANG=en_US.UTF-8
```

## Font configuration

Install the `terminus-font` package.

Edit the `/etc/conf.d/consolefont` file, set the `consolefont` variable
accordingly:

```
consolefont="ter-120n"
```

Enable service:

```
# rc-update add consolefont boot
```

## Hostname

Create the `/etc/hostname` file, set hostname accordingly:

```
myhostname
```

## Bootloader

See [GRUB](../archlinux-installation/basic/grub.md) for details.

## Networking

Install the `connman-openrc` package.

Enable service:

```
# rc-update add connmand default
```

## Sudo

See [Sudo](../sudo.md) for details.

Disable root login:

```
# passwd -l root
```

## Create user account

Create a user:

```
# useradd -m -G wheel <username>
```

Set password for the user:

```
# passwd <username>
```

## Additional packages

Packages:

- htop
- pacman-contrib

## Finish the installation

Exit the chroot environment:

```
# exit
```

Unmount all the partitions:

```
# umount -R /mnt
```

Shutdown the machine:

```
# poweroff
```

Remove the installation medium and start the machine.

## Post-installation

### Restore dotfiles if any

See the _Restore_ section in [Dotfiles](../dotfiles.md) for details.

### Editing files

Set the `EDITOR` variable accordingly:

```shell
$ export EDITOR=nvim
```

Use `sudoedit` to edit system configuration files.

### Set up a swap file if necessary

See [Swap file](../swap-file.md) for details.

### Firewall

- [Firewalld](../firewalld.md)
   - Install the `firewalld-openrc` package.
   - Enable service: `rc-update add firewalld default`
   - Start service: `rc-service firewalld start`
- [Uncomplicated firewall](../uncomplicated-firewall.md)
   - Install the `ufw-openrc` package.
   - Enable service: `rc-update add ufw default`
   - Start service: `rc-service ufw start`

### Use Dash as /bin/sh

See [Dash](../dash.md) for details.

### Arch repositories

Install the `artix-archlinux-support` package.

Edit the `/etc/pacman.conf` file, add the following lines:

```
# Arch repositories

[extra]
/etc/pacman.d/mirrorlist-arch

[community]
/etc/pacman.d/mirrorlist-arch
```

Reload default keys from the archlinux keyring:

```
# pacman-key --populate archlinux
```

### Reflector

Install the `reflector` package.

Update the mirror list (it may take a while):

```
# reflector -l 10 -p https --sort rate --save /etc/pacman.d/mirrorlist-arch
```

## References

1. [Installation guide][installation-guide]
2. [Change console font][change-console-font]
3. [Repositories][repositories]

[installation-guide]: https://wiki.artixlinux.org/Main/Installation
[change-console-font]: https://wiki.gentoo.org/wiki/Keyboard_layout_switching#OpenRC
[repositories]: https://wiki.artixlinux.org/Main/Repositories
