# List of programs

## Android file transfer support

Install the following packages:

- android-file-transfer
- android-udev

## Calculator

Install the `mate-calc` package.

## Disk management utility

Install the `gnome-disk-utility` package, the `exfat-utils` package for exFAT
support.

## Disk space cleaner

Install the `bleachbit` package.

## File archiver

Install the `engrampa` package.

## File comparison tool

Install the `meld` package.

## File manager

Install one of the following packages:

- nnn
- pcmanfm-gtk3

## Firewall

- [Firewalld](firewalld.md)
- [Uncomplicated firewall](uncomplicated-firewall.md)

## GTK configuration tool

Install the `lxappearance-gtk3` package and the following packages accordingly:

- breeze-gtk
- breeze-icons
- capitaine-cursors
- lxde-icon-theme
- mate-icon-theme
- mate-themes

## Image viewer

Install one of the following packages:

- gpicview
- sxiv

## Input method

See [Gcin](gcin.md) for details.

## Key binding management

Install the `sxhkd` package.

Edit the `~/.xprofile` file, make the daemon autostart:

```
sxhkd &
```

## Network applet

See the _System tray applet_ section in
[NetworkManager](../../networkmanager.md) for details.

## Notification daemon

Install the following packages:

- dunst
   - libnotify

Edit the `~/.xprofile` file, make the daemon autostart:

```
dunst &
```

## Password manager

Install the `bitwarden` package.

## Photo editor

Install the `gimp` package.

## Polkit authentication agent

Install the `mate-polkit` package.

Edit the `~/.xprofile` file, make the agent autostart:

```
/usr/lib/mate-polkit/polkit-mate-authentication-agent-1 &
```

## Text editor

Install one of the following packages:

- featherpad
- mousepad

## Trash support

Install the `trash-cli` package.

## Video player

Install the `mpv` package.

## Virtualization

- [Virt-manager](virt-manager.md)
- [VirtualBox](virtualbox.md)

## Volume applet

Install the following packages:

- pasystray
   - pavucontrol

Edit the `~/.xprofile` file, make the applet autostart:

```
pasystray &
```

## Wallpaper setting utility

Install the following packages:

- archlinux-wallpaper
- xwallpaper

Edit the `~/.xprofile` file, set wallpaper automatically:

```
xwallpaper --zoom /usr/share/backgrounds/archlinux/simple.png
```

## Web browser

See [Firefox](firefox.md) for details.

## References

1. [Xprofile][xprofile]

[xprofile]: https://wiki.archlinux.org/title/Xprofile
