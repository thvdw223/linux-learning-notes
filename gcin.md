# Gcin

## Installation

Install the `gcin` package.

## Configuration

Edit the `~/.xprofile` file, add the following lines:

```
export LC_CTYPE=zh_TW.UTF-8
export XMODIFIERS=@im=gcin
export GTK_IM_MODULE=gcin
export QT_IM_MODULE=gcin
LANG=zh_TW.UTF-8 gcin &
```

### Color theming

Change color settings via the GUI (Settings/Tool > Appearance Settings):

- Foreground color: `#000000`
- Background color: `#FFFFFF`
- Color of selection key: `#3399FF`
- Cursor color: `#3399FF`

## References

1. [Gcin][archwiki-gcin]

[archwiki-gcin]: https://wiki.archlinux.org/title/Gcin_(%E6%AD%A3%E9%AB%94%E4%B8%AD%E6%96%87)
