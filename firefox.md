# Firefox

## Installation

Install the `firefox` package.

## Privacy security

Enter _about:config_ in the Firefox address bar and press enter.

Press the **Accept the Risk and Continue** button, then set values to the
following configurations:

- Battery status:
   - `dom.battery.enabled = false`
- Clipboard events:
   - `dom.event.clipboardevents.enabled = false`
- Fingerprinting:
   - `privacy.resistFingerprinting = true`
- Firefox account:
   - `identity.fxaccounts.enabled = false`
- First party isolate:
   - `privacy.firstparty.isolate = true`
- Geolocation:
   - `geo.enabled = false`
- Microphone and camera status:
   - `media.navigator.enabled = false`
- Pocket:
   - `extensions.pocket.enabled = false`
- Referer header:
   - `network.http.sendRefererHeader = 0`
- WebRTC:
   - `media.peerconnection.ice.default_address_only = true`

## Profiles

Manage Firefox profiles:

```shell
$ firefox -P
```

Start Firefox with a different profile:

```shell
$ firefox -P <profile_name> --no-remote
```

## References

1. [Privacy][archwiki-privacy]
2. [Advanced preferences][advanced-preferences]
3. [Firefox privacy][firefox-privacy]
4. [Privacy tweeks][privacy-tweeks]
5. [Referrer][referrer]
6. [Using profiles][using-profiles]

[advanced-preferences]: https://github.com/sunknudsen/privacy-guides/tree/master/how-to-mitigate-fingerprinting-and-ip-leaks-using-firefox-advanced-preferences
[archwiki-privacy]: https://wiki.archlinux.org/title/Firefox/Privacy
[firefox-privacy]: https://restoreprivacy.com/firefox-privacy/
[privacy-tweeks]: https://wiki.mozilla.org/Privacy/Privacy_Task_Force/firefox_about_config_privacy_tweeks
[referrer]: https://wiki.mozilla.org/Security/Referrer
[using-profiles]: https://github.com/sunknudsen/privacy-guides/tree/master/how-to-use-multiple-compartmentalized-firefox-instances-simultaneously-using-profiles
