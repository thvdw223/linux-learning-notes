# Console fonts

The following instructions use the Terminus font. Make sure the `terminus-font`
package is installed. The console fonts are located in
`/usr/share/kbd/consolefonts/`.

## Have a bigger font

List the Terminus font:

```shell
$ ls /usr/share/kbd/consolefonts/ | grep 'ter-1.*'
```

The fonts with the _ter-1_ prefix are covered ISO8859-1, ISO8859-15 and
Windows-1252 codepages. See _README_ from
[Terminus font project][terminus-font-project] for details.

Set the font (e.g. `ter-120n.psf.gz`) temporarily:

```shell
$ setfont ter-120n
```

## Persistent configuration

Create the `/etc/vconsole.conf` file, set the `FONT` variable accordingly:

```
FONT=ter-120n
```

## References

1. [Console fonts][linux-console-fonts]
2. [Terminus font project][terminus-font-project]

[linux-console-fonts]: https://wiki.archlinux.org/title/Linux_console#Fonts
[terminus-font-project]: https://sourceforge.net/projects/terminus-font/
