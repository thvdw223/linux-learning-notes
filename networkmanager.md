# NetworkManager

## Installation

Install the `networkmanager` package.

Enable service:

```
# systemctl enable NetworkManager.service
```

## System tray applet

Install the `network-manager-applet` package.

Edit the `~/.xprofile` file, make the applet autostart:

```
nm-applet &
```

## Network bridge

Creating a bridge with STP disabled:

```
# nmcli connection add type bridge ifname br0 stp no
```

Find the interface name:

```shell
$ ip link show
```

Make interface (e.g. `enp0s3`) a slave to the bridge:

```
# nmcli connection add type bridge-slave ifname enp0s3 master br0
```

Find the current connection:

```shell
$ nmcli connection show --active
```

Set the existing connection as down:

```
# nmcli connection down <existing_connection>
```

Set the new bridge as up:

```
# nmcli connection up bridge-br0
```

## References

1. [NetworkManager][archwiki-networkmanager]
2. [Network bridge][archwiki-network-bridge]

[archwiki-network-bridge]: https://wiki.archlinux.org/title/Network_bridge#With_NetworkManager
[archwiki-networkmanager]: https://wiki.archlinux.org/title/NetworkManager
