# VirtualBox

## Installation

Install the following packages:

- virtualbox
   - virtualbox-host-dkms: for any other kernel (including linux-lts)
   - virtualbox-host-modules-arch: for the linux kernel

## USB devices

Porting the USB devices on VirtualBox requires the user to be a member of the
`vboxusers` group:

```shell
$ sudo usermod -aG vboxusers $USER
```

Download the _Extension Pack_ from [VirtualBox][virtualbox-download], then
install it via the GUI (File > Preferences > Extensions).

## Guest additions

Install the `virtualbox-guest-iso` package. The disc image is located at
`/usr/lib/virtualbox/additions/VBoxGuestAdditions.iso`.

### Arch Linux as guest

Install the `virtualbox-guest-utils` package.

Enable service:

```
# systemctl enable vboxservice.service
```

## Shared folder

Make sure the _guest additions_ is installed in guest machine.

Accessing a shared folder requires the user in guest machine to be a member of
the `vboxsf` group:

```shell
$ sudo usermod -aG vboxsf $USER
```

Create a shared folder via the GUI (Machine > Settings > Shared Folders), make
it auto-mount. The shared folder will be located in the `/media` directory and
its name starts with `sf_`.

## References

1. [VirtualBox][archwiki-virtualbox]

[archwiki-virtualbox]: https://wiki.archlinux.org/title/VirtualBox
[virtualbox-download]: https://www.virtualbox.org/wiki/Downloads
