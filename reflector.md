# Reflector

## Installation

Install the `reflector` package.

## Configuration

Edit the `/etc/xdg/reflector/reflector.conf` file, set the options accordingly:

```
--save /etc/pacman.d/mirrorlist
--protocol https
--latest 20
--sort rate
```

Update the mirror list (it may take a while):

```
# systemctl start reflector.service
```

Check errors if any:

```
# systemctl status reflector.service
```

## Automation

Update the mirror list weekly:

```
# systemctl enable reflector.timer
```

## References

1. [Reflector][archwiki-reflector]

[archwiki-reflector]: https://wiki.archlinux.org/title/Reflector
