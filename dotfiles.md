# Dotfiles

The following instructions use `$HOME/.dotfiles.git` as the example directory
to store a Git bare repository.

## Store

Create a git bare repository:

```shell
$ git init --bare $HOME/.dotfiles.git
```

Create an alias, add to your shell configurations:

```shell
$ alias dotfile='/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME'
```

Hide files we are not explicitly tracking yet:

```shell
$ dotfile config --local status.showUntrackedFiles no
```

Use `dotfile` as `git` to manage your dotfiles.

## Restore

Create an alias in the current shell scope:

```shell
$ alias dotfile='/usr/bin/git --git-dir=$HOME/.dotfiles.git --work-tree=$HOME'
```

Create the `$HOME/.gitignore` file, ignore the `.dotfiles.git` directory:

```shell
$ echo '.dotfiles.git/' > $HOME/.gitignore
```

Clone your dotfiles into a bare repository in the `$HOME/.dotfiles.git`
directory:

```shell
$ git clone --bare <repo> $HOME/.dotfiles.git
```

Checkout the actual content from the bare repository to your `$HOME`:

```shell
$ dotfile checkout
```

The step above might fail as some files would be overwritten by Git. Back up
the files if you need them. Then delete them and re-run the check out again.

Hide files we are not explicitly tracking yet:

```shell
$ dotfile config --local status.showUntrackedFiles no
```

## References

1. [How to store dotfiles][how-to-store-dotfiles]

[how-to-store-dotfiles]: https://www.atlassian.com/git/tutorials/dotfiles
