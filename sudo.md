# Sudo

Edit the `/etc/sudoers` file by:

```
# EDITOR=the-editor visudo
```

## Wheel group

Find the following line and uncomment it:

```
%wheel ALL=(ALL:ALL) ALL
```

## Password prompt timeout

Disable password prompt timeout:

```
Defaults passwd_timeout=0
```

## Timestamp timeout

Set number of minutes that can elapse before `sudo` will ask for a password
again:

```
Defaults timestamp_timeout=10
```

## Insults

Enable the insults easter egg:

```
Defaults insults
```

## References

1. [Sudo][archwiki-sudo]

[archwiki-sudo]: https://wiki.archlinux.org/title/Sudo
