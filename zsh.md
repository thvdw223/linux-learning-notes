# Zsh

## Installation

Install the following packages:

- zsh
- zsh-completions

## Default shell

List all installed shells:

```shell
$ chsh -l
```

Set Zsh as your default shell:

```shell
$ chsh -s /usr/bin/zsh
```

Log out and log in again, the default shell will be Zsh.

## Syntax highligting

Install the `zsh-syntax-highlighting` package.

Add the following lines to the end of your `.zshrc` file:

```
[ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ] && \
	source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
```

## References

1. [Zsh][archwiki-zsh]

[archwiki-zsh]: https://wiki.archlinux.org/title/Zsh
