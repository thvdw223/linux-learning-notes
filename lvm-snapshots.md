# LVM snapshots

## Create

Create a snapshot:

```
# lvcreate -L <size> -s -n <name> /dev/<volume_group>/<target_volume>
```

The size of a snapshot can be estimated with the room for the changes which the
target volume makes.

The naming scheme for a snapshot as follow:

```
<target_volume>.<mmdd>.ss
```

## Display information

Display detailed information of logical volume:

```
# lvdisplay
```

Display summary information of logical volume:

```
# lvs
```

## Revert

Revert the modified logical volume to the state when the snapshot was taken:

```
# lvconvert --merge /dev/<volume_group>/<snapshot_name>
```

The snapshot will no longer exist after merging. Reboot the machine if
necessary.

## Delete

Delete a snapshot:

```
# lvremove /dev/<volume_group>/<snapshot_name>
```

## References

1. [Managing LVM Snapshots on Arch Linux][learnlinuxtv-snapshots]
2. [Snapshots][archwiki-snapshots]

[archwiki-snapshots]: https://wiki.archlinux.org/title/LVM#Snapshots
[learnlinuxtv-snapshots]: https://www.youtube.com/watch?v=RnjpLZmQ4DM
