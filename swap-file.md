# Swap file

## Swap file creation

Create a 1 GiB swap file:

```
# dd if=/dev/zero of=/swapfile bs=1M count=1024 status=progress
```

Set the right permissions:

```
# chmod 600 /swapfile
```

Format it to swap:

```
# mkswap /swapfile
```

Backup the `/etc/fstab` file:

```
# cp /etc/fstab /etc/fstab.bak
```

Edit the `/etc/fstab` file, add the following line:

```
/swapfile	none	swap	defaults	0 0
```

Check the `/etc/fstab` file for errors:

```
# mount -a
```

Enable swap:

```
# swapon /swapfile
```

## References

1. [Swap file][archwiki-swap-file]

[archwiki-swap-file]: https://wiki.archlinux.org/title/Swap#Swap_file
