# Virt-manager

A graphical front-end for libvirt.

## Installation

Install the following packages:

- libvirt
   - dnsmasq
   - iptables-nft: conflict with the `iptables` package
   - qemu
- virt-manager

Enable service:

```
# systemctl enable libvirtd.service
```

## Access to libvirt

The easiest way to ensure your user has access to libvirt daemon is to add
member to `libvirt` user group:

```shell
$ sudo usermod -aG libvirt $USER
```

## Create a network bridge if necessary

See the _Network bridge_ section in [NetworkManager](networkmanager.md) for
details.

## Arch Linux as guest

### Video card

Select **Virtio** as the guest's video card via the GUI (View > Details).

### QEMU guest agent

In the guest's details window, make sure there is a channel called _qemu-ga_
and its values as follows:

- Device type: `unix`
- Target name: `org.qemu.guest_agent.0`

Install the `qemu-guest-agent` package.

Enable service:

```
# systemctl enable qemu-guest-agent.service
```

### SPICE agent

In the guest's details window, make sure there is a channel called _spice_ and
its values as follows:

- Device type: `spicevmc`
- Target name: `com.redhat.spice.0`

Install the `spice-vdagent` package.

The following steps are for those who don't use display managers.

Edit the `~/.xprofile` file, make the agent autostart:

```
spice-vdagent &
```

Tick on the **Auto resize VM with window** option via the GUI (View > Scale
Display) after running `startx`. Quit the graphical environment and re-run
`startx`, the guest's resolution should be adjusted to the size of window.

### Shared folder

Open the virtual machine details window via the GUI (Edit > Virtual Machine
Details > View > Details).

Click the **Add hardware** button, then select the **Filesystem**.

Choose `virtio-9p` in the driver option.

In the source path, click the **Browse...** button, then the **Browse Local**
button and select the directory you want to share with the guest machine.

The target path is a mount tag in this case. You can name it anything (e.g.
`shared_mount`).

Create the `~/Shared` directory in the guest machine, then mount the shared
directory:

```shell
$ sudo mount -t 9p -o trans=virtio,version=9p2000.L shared_mount ~/Shared
```

To mount it at boot, add it to the guest's fstab:

```
shared_mount	/home/username/Shared	9p	trans=virtio,version=9p2000.L	0 0
```

Edit the `/etc/modules-load.d/9pnet_virtio.conf` file, preload the module
during boot:

```
9pnet_virtio
```

## References

1. [Libvirt][archwiki-libvirt]
2. [QEMU][qemu]
3. [Setting up shared folders][distrotube-shared-folders]

[archwiki-libvirt]: https://wiki.archlinux.org/title/Libvirt
[qemu]: https://wiki.archlinux.org/title/QEMU
[distrotube-shared-folders]: https://www.youtube.com/watch?v=9FBhcOnCxM8&t=431s
